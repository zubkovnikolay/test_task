<?php

/**
 * User: Nikolay Zubkov
 * Date: 05.01.2022
 * Time: 22:27
 */

require './functions.php';

$content = file_get_contents('./gendalf.png');
header('Content-Type: image/gif');
echo $content;

$visitorInfo = getVisitorInfo();

if (getViewsCount($visitorInfo) === 0) {
    registerView($visitorInfo);
} else {
    incrementViewsCount($visitorInfo);
}