<?php

/**
 * User: Nikolay Zubkov
 * Date: 05.01.2022
 * Time: 22:50
 */

require 'Db.php';

/**
 * @param $visitorInfo
 * @return int
 */
function getViewsCount($visitorInfo): int
{
    $connection = Db::getInstance()->getConnection();
    $stmt = $connection->prepare('
            SELECT
                   views_count
            FROM visitors
            WHERE ip_address = :ip_address AND user_agent = :user_agent AND page_url = :page_url
            '
    );
    $stmt->execute($visitorInfo);
    
    $result = $stmt->fetchAll();
    
    return $result[0]['views_count'] ?? 0;
}


/**
 * @param $visitorInfo
 * @return void
 */
function registerView($visitorInfo): void
{
    $connection = Db::getInstance()->getConnection();
    $stmt = $connection->prepare('
            INSERT INTO visitors (ip_address, user_agent, page_url, views_count)
            VALUES (:ip_address, :user_agent, :page_url, 1)
            '
    );
    $stmt->execute($visitorInfo);
}


/**
 * @param $ip
 * @param $pageUrl
 * @param $userAgent
 * @return void
 */
function incrementViewsCount($visitorInfo): void
{
    $connection = Db::getInstance()->getConnection();
    $stmt = $connection->prepare('
            UPDATE visitors SET views_count = views_count + 1, view_date = current_timestamp
            WHERE ip_address = :ip_address AND user_agent = :user_agent AND page_url = :page_url
                '
    );
    $stmt->execute($visitorInfo);
}

/**
 * Returns array of visitor's info: ip, user-agent and page url in pointed order
 * @return array
 */
function getVisitorInfo(): array
{
    //выбрать посещение по ip, user-agent'у и url'у страницы
    $ip = getUserIp();
    $pageUrl = $_SERVER['HTTP_REFERER'];
    $userAgent = $_SERVER['HTTP_USER_AGENT'];
    
    return ['ip_address' => $ip, 'page_url' => $pageUrl, 'user_agent' => $userAgent];
}

/**
 * @return string
 */
function getUserIp(): string
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    
    return $ip;
}
