<?php

/**
 * User: Nikolay Zubkov
 * Date: 05.01.2022
 * Time: 22:54
 */

class Db
{
    private static $instance;
    
    private $connection;
    
    private $user = 'test';
    
    private $pass = 'test';
    
    private $dsn = "mysql:host=127.0.0.1;dbname=test;charset=utf8";
    
    private $opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];
    
    /**
     *
     */
    private function __construct()
    {
        $this->connection = new PDO($this->dsn, $this->user, $this->pass, $this->opt);
    }
    
    /**
     * @return Db
     */
    public static function getInstance(): Db
    {
        if (!self::$instance) {
            self::$instance = new self;
        }
        
        return self::$instance;
    }
    
    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->connection;
    }
}
